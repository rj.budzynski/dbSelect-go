package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"time"

	"github.com/apcera/termtables"
	"github.com/go-ini/ini"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/logrusorgru/aurora"
)

const usage = `Usage:

	%s [OPTS] [FILE]

OPTS is one of:
	-c | --csv : CSV output
	-l | --long : "long" output
	-h | --help : print this message

Output is written to stdout.
The default is a "pretty-printed" tabular form.
FILE contains a single SQL query (SELECT ...).
If no argument is given, the query is read from stdin.
`

func getConnString(cnfFile string) (string, error) {
	cfg, err := ini.Load(cnfFile)
	if err != nil {
		return "", err
	}
	client := cfg.Section("client")
	user := client.Key("user").String()
	passwd := client.Key("password").String()
	host := client.Key("host").String()
	db := client.Key("database").String()
	return fmt.Sprintf("%s:%s@tcp(%s)/%s?interpolateParams=true&timeout=5s&parseTime=true&collation=utf8mb4_polish_ci",
		user, passwd, host, db), nil
}

func main() {
	var (
		tab *termtables.Table
		cw  *csv.Writer
	)
	prog, args := path.Base(os.Args[0]), make([]string, 0)
	mode := "pretty"
Loop:
	for i, a := range os.Args[1:] {
		switch a {
		case "-h", "--help":
			fmt.Printf(usage, prog)
			return
		case "-c", "--csv":
			mode = "csv"
		case "-l", "--long":
			mode = "long"
		default:
			if a == "--" {
				args = append(args, os.Args[i+2:]...)
				break Loop
			}
			if strings.HasPrefix(a, "-") {
				fmt.Fprintf(os.Stderr, "%s: %s: unrecognized option.\n%s", prog, a, usage)
				os.Exit(1)
			}
			args = append(args, a)
		}
	}
	f := os.Stdin
	if len(args) > 1 {
		fmt.Fprintf(os.Stderr,
			"%s: warning: query will be read from first argument only.\n", prog)
	}
	if len(args) > 0 {
		f, _ = os.Open(args[0])
	}
	q, err := ioutil.ReadAll(io.Reader(f))
	panicErr(err)
	query := string(q)
	query = strings.TrimSpace(query)
	query = strings.TrimRight(query, ";")
	connString, err := getConnString(path.Join(os.Getenv("HOME"), "/.usosweb.cnf"))
	panicErr(err)
	db := sqlx.MustConnect("mysql", connString)
	defer db.Close()
	rows, err := db.Queryx(query)
	panicErr(err)
	fields, _ := rows.Columns()
	switch mode {
	case "pretty":
		termtables.EnableUTF8PerLocale()
		tab = termtables.CreateTable()
		for _, name := range fields {
			tab.AddHeaders(aurora.Bold(name))
		}
	case "csv":
		cw = csv.NewWriter(os.Stdout)
		cw.Write(fields)
		cw.Flush()
		panicErr(cw.Error())
	case "long":
	}

	var nrows uint
	for rows.Next() {
		cols, _ := rows.SliceScan()
		for i, v := range cols {
			switch v := v.(type) {
			case time.Time:
				cols[i] = v.Format("2006-01-02")
			case []byte:
				cols[i] = fmt.Sprintf("%s", v)
			case nil:
				cols[i] = "NULL"
			default:
				cols[i] = fmt.Sprintf("%v", v)
			}
		}
		switch mode {
		case "pretty":
			tab.AddRow(cols...)
		case "csv":
			for i, v := range cols {
				s, ok := v.(string)
				if ok {
					fields[i] = s
				} else {
					fields[i] = "HUH?"
				}
			}
			cw.Write(fields)
			cw.Flush()
			panicErr(cw.Error())
		case "long":
			fmt.Printf("\n*** ROW %d ***\n", nrows+1)
			for i, v := range cols {
				fmt.Printf("%s: %s\n", fields[i], v)
			}
		}
		nrows++
	}
	if err = rows.Err(); err != nil {
		fmt.Printf("query error: %v\n", err)
		os.Exit(2)
	}
	if nrows > 0 {
		switch mode {
		case "pretty":
			fmt.Println(tab.Render())
			fmt.Printf("query returned %d rows.\n", nrows)
		case "csv":
		case "long":
		}
	} else {
		fmt.Println("query returned no rows.")
	}
}

func panicErr(err error) {
	if err != nil {
		panic(err)
	}
}
